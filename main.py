from datetime import timedelta
from importlib.resources import path
from urllib import request, response

from starlette.responses import HTMLResponse

from fastapi import Path, Query, Body, Form, File, UploadFile
from fastapi import FastAPI, Query
from fastapi import Response, Request, Depends, status
from fastapi.encoders import jsonable_encoder
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.responses import RedirectResponse
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.responses import FileResponse, HTMLResponse, StreamingResponse

import os
from db import personajes, tecnologias


app = FastAPI()
templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/")
def get_index(request: Request):
    return templates.TemplateResponse("index.html", {"request": request, "title": "Home"})

#@app.get("/Personajes")
#def get_Personajes(request:Request):
#    return templates.TemplateResponse("Personajes.html", {"request":request,"personajes": response, "title":"Personajes"})

@app.get(path="/Personajes",
         response_class=HTMLResponse,
         #response_model=List[Dict[str, Car]],
         tags=["Get"],
         summary="Lista de personajes",
         status_code=status.HTTP_200_OK
         )
def get_cars(request: Request, number: str = Query("10", max_length=3)):
    response = []
    for id, PersonajeBase in list(personajes.items())[:int(number)]:
        response.append((id,PersonajeBase))
    return templates.TemplateResponse("Personajes.html", {"request":request, "personajes":response, "title":"Personajes"})

@app.get(path="/Tecnologias",
         response_class=HTMLResponse,
         tags=["Get"],
         summary="Las Tecnologias mensionadas",
         status_code=status.HTTP_200_OK
)
def get_Tecno(request: Request, number: str = Query("10", max_length=3)):
    response = []
    for id, tecnologia in list(tecnologias.items())[:int(number)]:
        response.append((id,tecnologia))
    return templates.TemplateResponse("Tecnologias.html", {"request":request, "tecnologias":response, "title":"Tecnologias"})

@app.get("/Aventura")
def get_aventura(request: Request):
    return templates.TemplateResponse("Aventura.html", {"request": request, "title": "Aventura"})

@app.get("/Reseña")
def get_resena(request: Request):
    return templates.TemplateResponse("Reseña.html", {"request": request, "title": "Aventura"})





    