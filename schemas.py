from email.mime import image
from turtle import st
from pydantic import BaseModel
from typing import List, Optional

class PersonajeBase(BaseModel):
    name: str
    Descripcion: str
    Profesion: str
    Rol: str
    Caracteristicas: str
    foto: str
    
class TecnologiasBase(BaseModel):
    NameTecno: str
    Descripcion: str
    Aparicion: str
    Usos: str
    Ano: int
    imagen: str